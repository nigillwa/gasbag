//
//   @file    AtlasUtils.h         
//   
//
//   @author M.Sutton
// 
//   Copyright (C) 2010 Atlas Collaboration
//
//   $Id: AtlasUtils.h 116199 2013-09-24 08:19:11Z amccarn $


#ifndef __ATLASUTILS_H
#define __ATLASUTILS_H

#include "TGraphErrors.h"
#include "TGraphAsymmErrors.h"

void ATLAS_LABEL(Double_t x,Double_t y,Color_t color=1); 

TGraphErrors* myTGraphErrorsDivide(TGraphErrors* g1,TGraphErrors* g2);

TGraphAsymmErrors* myTGraphErrorsDivide(TGraphAsymmErrors* g1,TGraphAsymmErrors* g2);

TGraphAsymmErrors* myMakeBand(TGraphErrors* g0, TGraphErrors* g1,TGraphErrors* g2);

void myAddtoBand(TGraphErrors* g1, TGraphAsymmErrors* g2);

TGraphErrors* TH1TOTGraph(TH1 *h1);

void myText(Double_t x,Double_t y,Color_t color,const char *text,float tsize=0.05);

void myBoxText(Double_t x, Double_t y,Double_t boxsize,Int_t mcolor,const char *text,Double_t style=1001);

void myMarkerText(Double_t x,Double_t y,Int_t color,Int_t mstyle,const char *text,Float_t msize=2.); 

void myLineText( Double_t x, Double_t y, Double_t boxsize,Int_t mcolor,const char *text, float tsize=0.05);

void myLineTextV(Double_t x, Double_t y, Double_t boxsize,Int_t mcolor,const char *text, int lineStyle = 0,  float tsize=0.05);

#endif // __ATLASUTILS_H
