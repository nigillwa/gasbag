# The name of the input file containing the input MC template 
inFileName = "../krigitSpuriousSig/GPRTesting_HGamCouplings/HGamCoupTemplates/bkg_templates.root" 

# The name of the histogram containing the unsmoothed template
templateHistoNames = [ 	"Template_ggH_0J_Cen" ,
						"Template_ggH_0J_Fwd" , 
						"Template_ggH_1J_LOW" , 
						"Template_ggH_1J_MED" , 
						"Template_ggH_1J_HIGH" ,
						"Template_ggH_1J_BSM" ,
						"Template_ggH_2J_LOW" , 
						"Template_ggH_2J_MED" , 
						"Template_ggH_2J_HIGH" ,
						"Template_ggH_2J_BSM" ,
						"Template_VBF_HjjLOW_loose" , 
						"Template_VBF_HjjLOW_tight" , 
						"Template_VBF_HjjHIGH_loose" , 
						"Template_VBF_HjjHIGH_tight" , 
						"Template_VHhad_loose" , 
						"Template_VHhad_tight" , 
						"Template_qqH_BSM" , 
						"Template_VHMET_LOW" , 
						"Template_VHMET_HIGH" , 
						"Template_VHlep_LOW" , 
						"Template_VHlep_HIGH" , 
						"Template_VHdilep" ]

# A list of the names of the categories for which templates have been provided (optional) 
#   If empty, will default to the above list of template names 
categoryNames = [ 	"ggH 0J Central" , 
					"ggH 0J Forward" , 
					"ggH 1J Low" ,
					"ggH 1J Medium" , 
					"ggH 1J High" , 
					"ggH 1J BSM" , 
					"ggH 2J Low" ,
					"ggH 2J Medium" , 
					"ggH 2J High" , 
					"ggH 2J BSM" , 
					"VBF H_{jj}^{Low} Loose" , 
					"VBF H_{jj}^{Low} Tight" , 
					"VBF H_{jj}^{High} Loose" , 
					"VBF H_{jj}^{High} Tight" , 
					"VHhad Loose" , 
					"VHhad Tight" , 
					"qqH BSM" , 
					"VHMET Low" , 
					"VHMET High" , 
					"VH lep Low" , 
					"VH lep High" , 
					"VH dilep" ]

# The name of the input file containing the data sidebands (optional) 
#   This can be the same as the filename containing the bkg templates , or it can be blank 
#   if the user does not want to look at the data sidebands 
#inDataSBFileName = ""
inDataSBFileName = "inTemplates/bkg_templates.root" 

# The name of the histograms containing the sideband data events (optional) 
#   This can be left empty. Otherwise, the sidebands should correspond to the categories given above
#sideBandRefNames = ""
sideBandRefNames = [ 	"Msb_ggH_0J_Cen" ,
						"Msb_ggH_0J_Fwd" , 
						"Msb_ggH_1J_LOW" , 
						"Msb_ggH_1J_MED" , 
						"Msb_ggH_1J_HIGH" ,
						"Msb_ggH_1J_BSM" ,
						"Msb_ggH_2J_LOW" , 
						"Msb_ggH_2J_MED" , 
						"Msb_ggH_2J_HIGH" ,
						"Msb_ggH_2J_BSM" ,
						"Msb_VBF_HjjLOW_loose" , 
						"Msb_VBF_HjjLOW_tight" , 
						"Msb_VBF_HjjHIGH_loose" , 
						"Msb_VBF_HjjHIGH_tight" , 
						"Msb_VHhad_loose" , 
						"Msb_VHhad_tight" , 
						"Msb_qqH_BSM" , 
						"Msb_VHMET_LOW" , 
						"Msb_VHMET_HIGH" , 
						"Msb_VHlep_LOW" , 
						"Msb_VHlep_HIGH" , 
						"Msb_VHdilep" ]

# Whether or not to calculate systematic error contribution by varying the length scale 
calculateSystematics = True 

# The name of the file containing the optimized Gibbs hyper-parameter ranges for each category 
hyperParFileName = "GPR_HyperParameter_Optimization.root"

# The hyper-parameter ranges to be used, if not using the outputs of the hyper-par optimization 
Gibbs_l0_bounds = [8,12]
Gibbs_l_slope_bounds = [0.01,0.5]

# The number of bins to combine, if rebinning input histogram (optional, but can make the GPR fit run faster if the input histogram has very fine binning) 
nRebin = 1

# The name of the output file containing the GPR-smoothed template 
# If this is left blank, then the ranges provided in the next options will be used, instead 
outTemplateFileName = "GPR_Smoothed_Templates.root" 

# Option to save the original templates (and data sidebands, if provided) in the output file 
saveInputHistograms = True 