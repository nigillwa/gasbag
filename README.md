This package is designed to apply Gaussian Process Regression to Monte Carlo or data
CR background-only templates in order smooth out statistical fluctuations. Note 
that this is NOT designed to do a full signal+background fit in a signal region. 

In order to run, you'll need the following packages: 
- ROOT 6 with PyROOT
- Python 2.7+ (2.7 recommended on lxplus)
- scikit-learn 0.20.2+
- scipy 1.1.0+ (Note: only 1.1.0 works on lxplus)
- numpy 1.14.2+

NOTE: Due to recent changes in lxplus, it is likely easier to use lcgenv to set 
up the necessary packages than a virtual environment. The virtual env instructions 
are still provided below in case you prefer to stay on SLC6. To setup up your 
environment on lxplus with SLC7 and lcgenv, just do: 
setupATLAS <br />
lsetup "lcgenv -p LCG_95apython3 x86_64-centos7-gcc7-opt Python" <br />
lsetup "lcgenv -p LCG_95apython3 x86_64-centos7-gcc7-opt ROOT" <br />
lsetup "lcgenv -p LCG_95apython3 x86_64-centos7-gcc7-opt numpy" <br />
lsetup "lcgenv -p LCG_95apython3 x86_64-centos7-gcc7-opt scikitlearn" <br />
lsetup "lcgenv -p LCG_95apython3 x86_64-centos7-gcc7-opt scipy" <br />


(DEPRECATED) If you want to run on lxplus, you will need to set up a virtual environment as follows: 

setupATLAS   <br />
lsetup "root 6.12.06-x86_64-slc6-gcc62-opt"   <br />
virtualenv myVirtualEnv --python=python2.7   <br />
cd myVirtualEnv   <br />
source bin/activate   <br />
pip install numpy   <br />
pip install scipy==1.1.0   <br />
pip install scikit-learn   <br />


Applying GPR to Smooth an Existing Background Template: 
The python script "smoothBackgroundTemplates.py" will apply a GPR fit to the 
background templates, as specified in the file "inputs_SmoothBackgroundTemplates.py". 
Use the options here to specify the input file name, template names, 
hyper parameters, etc. The script will produce a template with identical binning to 
the input template. However, the GPR fitting can be somewhat slow if a very large 
number of bins are provided. In this case, one can change the "nRebin" variable to 
someting greater than one to merge bins together. This will grealy speed up the 
template making process. 
In addition, the script assumes the GPR prior (very rough aproximation of the 
background shape) is a falling exponential function. This may need to be changed 
for analyses which have significantly different background shapes. Note that the 
prior just needs to be a **very rough** approximation of the background shape; if one 
knew the exact analytical shape, then this script would not be necessary! 


Obtaining Optimal Hyper Parameters: 
The pyhton script "optimizeHyperPars.py" will apply many GPR fits with different 
hyper parameters to a smooth functional fit to the background templates, as 
specified in "inputs_OptimizeHyperPars.py". Use the options here to specify the 
input file name, template names, hyper parameters to be tested, etc. The available 
fit functions (for generating a smooth shape) are: 
- "PowerLaw" 
- "Exponential" 
- "ExpPoly2" 
- "ExpPoly3" 
- "Bern3" 
- "Bern4" 
- "Bern5"
If your background template is very different, then you may need to add an additional 
function to the code. Once the code has run, a 2D histogram will be produced mapping 
the 2D Gibbs hyper-parameter space tested. The red area indicates good hyper-parameters, 
while the blue area indicates not-as-good hyper-parameters. The "good" hyper 
parameters are chosen such that narrow features O~bin width are smoothed out by 
at least 33%, while wider features of O~expected signal width are smoothed out by 
no more than 25%. The latter criteria is to ensure that if some real, signal like 
feature is sculpted by the analysis cuts on the background shape, this real spurious 
signal will not be removed. 


